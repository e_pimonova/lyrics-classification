from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import pickle
import os

#собираем список жанров с сайта lyrics.com 
def get_genres():
    doc = BeautifulSoup(urlopen(r"https://www.lyrics.com/genre/Blues"), features="html.parser")

    result = [item['data-link'].replace('Blues;', '') for item in doc('button', attrs={'class': "action btn xsml", 'data-tag': "genre"})] #т.к. на главной странице нет ссылок на жанры, забираем их со страницы с песнями жанра блюз (просто уберём 'Blues;' из значения тэга 'data-link'.
    doc= None
    result.insert(0, r'/genre/Blues')

    return result

#собираем песни с одной страницы
def do_one_page(genre, page_n, data_dict, errors): #на вход принимаем 1) жанр, в рамках которого мы сейчас собираем песни, 2) номер страницы, на которой мы находимся, 3) словарь, где ключ - название песни, а значение - слова песни, 4) список ошибок, куда попадают страницы, которые не получилось обработать
    print('do_one_page', genre, page_n)
    for i in range(10):
        try:
            soup = BeautifulSoup(urlopen(r'https://www.lyrics.com/genres.php?genre={}&p={}'.format(genre, page_n)), features="html.parser") #10 раз пытаемся забрать текущую страницу с песнями одного жанра
            break
        except:
            print('Soup error ', genre, page_n) #если 10 раз не хватило и страницу забрать не получилось, то выводим сообщение об ошибке и...
            soup = None

    if not soup:
        errors.append((genre, page_n, '', '')) #... добавляем взбунтовавшуюся страницу и её жанр в список ошибок
        return 0

    page_count = max([int(item.text) for item in soup('a', attrs={'class' : 'rc5'})])


    items = [(item['href'], item.text) for item in soup('a', attrs={'href' : re.compile(r"/lyric/[0-9]+/.*")})] #если до этого ошибок не было и в soup-е лежит не пустое значение, собираем список кортежей, в которых лежат ссылки на песни и названия песен.
    for href, title in items:
        if title not in data_dict:
            print(genre, page_n, title, href, end='')
            try:
                doc = BeautifulSoup(urlopen(r'https://www.lyrics.com'+href), features="html.parser") #заходим по ссылкам на песни
                lyric = doc('pre', attrs={'id':'lyric-body-text'})[0].text #запоминаем в lyric текст песни
                data_dict[title] = lyric #и добавляем в словарь data_dict по ключу-названию песни значение-текст песни
                print(' - ok')
            except:
                errors.append((genre, page_n, title, href)) #в противном случае (если не получилось зайти на страницу с песней) добавляем ее жанр, номер страницы, название и ссылку в список ошибок
                print(' - fail')
                

    soup = None
    items = None
    doc = None
    
    return page_count


if __name__ == '__main__':
    #genres = get_genres()
    #print(genres)

    # по-хорошему, мы можем вызвать написанную функцию get_genres и обрабатывать каждый жанр в списке, но в реальности оказалось, что процесс довольно долгий, поэтому можно вручную задавать каждый жанр на обработку
    genres = ['Stage%20__%20Screen'] # 
                        #'Funk%20--%20Soul', 'Hip%20Hop',
                        #'Jazz', #102
                        #'Latin', #46
                        #'Reggae', 'Non-Music', 'Rock', 'Spiritual', 'Stage%20__%20Screen']
    
        
    for g in genres:
        print('process', g)

        page_n = 1
        soup = BeautifulSoup(urlopen(r'https://www.lyrics.com/genres.php?genre={}&p={}'.format(g, page_n)), features="html.parser")
        page_count = max([int(item.text) for item in soup('a', attrs={'class' : 'rc5'})]) #определяем, сколько страниц с песнями текущего жанра
        soup = None

        content = {} #создаём наш главный словарь, где будут названия и слова песен
        errors = [] #и список ошибок
        
        while page_n <= page_count: #до тех пор, пока не пройдём по всем страницам текущего жанра, вызываем ф-цию do_one_page
            print('page_n',page_n)
            
            do_one_page(g, page_n, content, errors)
            page_n += 1

        with open(g+'_content.pkl', 'wb') as f: #и записываем словарь с песнями текущего жанра в файл pickle
            pickle.dump(content, f)

        with open(g+'_errors.pkl', 'wb') as f: #то же самое делаем для ошибок (на всякий случай, вдруг ошибки тоже пригодятся)
            pickle.dump(errors, f)
#Вуаля!